import { createApp } from 'vue'
import App from './App.vue'

// Import plugin modal vue-universal-modal
import VueUniversalModal from 'vue-universal-modal'
import 'vue-universal-modal/dist/index.css'

const app = createApp(App)
app.use(VueUniversalModal, {
  teleportTarget: '#modals',
  modalComponent: 'Modal'
})
// app.config.isCustomElement = tag => tag.startsWith('xml')
app.mount('#app')
