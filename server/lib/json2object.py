import json

def json2data(filename):
    try:
        thisFile = open(filename, "r")
        try:
            return json.loads(thisFile.read())
        except Exception as e:
            raise e
    except Exception as e:
        raise e
