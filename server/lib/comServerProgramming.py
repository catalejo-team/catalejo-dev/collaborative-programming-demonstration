import socket

def start_server2Programming(HOST='127.0.0.1', PORT=3000):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sck:
        sck.bind((HOST, PORT))
        sck.listen()
        conn, addr = sck.accept()
        with conn:
            print('Connected by', addr)
            while True:
                data = conn.recv(1024)
                if not data:
                    break
                conn.sendall(data)

class ConnServer2prog:
    def __init__(self, HOST='127.0.0.1', PORT=3000):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as self.sck:
            self.sck.bind((HOST, PORT))
            self.sck.listen()
            self.conn, self.addr = self.sck.accept()
    
    def send(self, data):
        with self.conn:
            self.conn.sendall(data)
